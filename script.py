import win32com.client
from configuration import output_dir, read_dir, FILE_MAIL
import os


def find_pst_folder(outlook_object, pst_filepath):
    for store in outlook_object.Stores:
        if store.IsDataFileStore and store.FilePath == pst_filepath:
            return store.GetRootFolder()
    return None

def save_message(object_to_work):
    try:
        indice, mensaje = object_to_work
        subject = mensaje.Subject
        body = mensaje.body
        attachments = mensaje.Attachments
        output_name = f"{indice} - {subject}"
        target_folder = os.path.join(output_dir, output_name)
        if not os.path.exists(target_folder):
            os.mkdir(target_folder)
        file_to_save = os.path.join(target_folder,FILE_MAIL)
        with open(file_to_save, "w") as file:
            file.write(str(body))
        for attach in attachments:
            path_name = os.path.join(target_folder, str(attach))
            attach.SaveAsFile(path_name)
    except Exception as e:
        print(f"ERROR {e}")

def create_iterator(pst_folder_object):
    object_to_work = iter(enumerate(pst_folder_object.Items))
    if object_to_work:
        while True:
            try:
                save_message(next(object_to_work))
            except StopIteration:
                break

def enumerate_folders(pst_folder_object) :
    for childfolder in pst_folder_object.Folders :
        enumerate_folders(childfolder)
    create_iterator(pst_folder_object)
    

outlook = win32com.client.Dispatch("Outlook.Application").GetNamespace("MAPI")
outlook.AddStore(read_dir)
pst_folder_object = find_pst_folder(outlook, read_dir)
pst_folders = enumerate_folders(pst_folder_object)
