import sqlite3
import os
import re
from configuration import DATABASE_NAME, current_directory

database = os.path.join(current_directory, DATABASE_NAME)
con = sqlite3.connect(database)
cur = con.cursor()
query = cur.execute("SELECT * FROM data;")
information = query.fetchall()
data_to_save = []
for i in information:
    path, name, pwd, data = i
    if name == "Solicitud Grupal 140":
        x = re.findall('\d+', data)
        id_persona = None
        for _ in x:
            if len(_) == 9:
                id_persona = _
                data_to_save.append((path, name, pwd, id_persona))

cur.execute("CREATE TABLE IF NOT EXISTS data_by_id(path, name, pwd, id_persona)")
for data in data_to_save:
    con.execute("INSERT INTO data_by_id VALUES (?,?,?,?)", data)
    con.commit()
con.close()
