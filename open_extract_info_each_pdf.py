import os
from configuration import output_dir, FILE_INFO, FILE_CONTENT, FILE_NAME
from PyPDF2 import PdfReader

def pdf_generator(url:str, pwd:str = ""):
    reader = PdfReader(url)
    if reader.is_encrypted and pwd:
        reader.decrypt(pwd)
    number_of_pages = len(reader.pages)
    for i in range(number_of_pages):
        page = reader.pages[i]
        yield page.extract_text()
        
        
def pdf_evaluator(url:str, text_to_find:str) -> bool:
    it = pdf_generator(url)
    while True:
        try:
            if(next(it).lower().find(text_to_find) >0):
                return True        
        except Exception:
            return False

def pdf_extractor(url:str, pwd:str) -> str:
    it = pdf_generator(url, pwd)
    text_to_response = ""
    while True:
        try:
            text_to_response += next(it).lower()
        except Exception:
            print("End process")
            break
        else:
            return text_to_response
        
names = [ os.path.join(output_dir,f"{x}") for x in os.listdir(output_dir)]

for name in names:
    with os.scandir(name) as it:
        for entry in it:
            read_data = None
            data_pdf = None
            if entry.name.endswith("pdf"):
                variable = os.path.join(name, FILE_INFO)
                if os.path.isfile(variable):
                    with open(variable, "r") as f:
                        read_data = f.readlines()
                pwd = read_data[2].replace("\n","")
                pdf_path = os.path.join(name, entry.name)
                value = pdf_extractor(pdf_path, pwd)
                file_to_put = os.path.join(name, FILE_CONTENT)
                
                with open(file_to_put, "w") as f:
                    f.write(value)
                
                with open(os.path.join(name, FILE_NAME), "w") as file:
                    data = entry.name.split(".pdf")[0].split(" ")[1] + "\n"
                    file.write(data)
