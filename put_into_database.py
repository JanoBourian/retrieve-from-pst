import sqlite3
import os
from configuration import output_dir, FILE_INFO, DATABASE_NAME, current_directory, FILE_CONTENT, FILE_NAME

database = os.path.join(current_directory, DATABASE_NAME)
con = sqlite3.connect(database)
cur = con.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS data(path, document, pwd, content)")

## Extract info for each document
names = [ os.path.join(output_dir,f"{x}") for x in os.listdir(output_dir)]

for name in names:
    with os.scandir(name) as it:
        path = None
        document = None
        pwd = None
        content = ""
        for entry in it:
            if entry.name.endswith(FILE_INFO):
                with open(os.path.join(name,FILE_INFO)) as file:
                    data = file.readlines()
                document = data[1].replace("\n", "")
                pwd = data[2].replace("\n", "")
            
            if entry.name.endswith(FILE_CONTENT):
                data = None
                with open(os.path.join(name,FILE_CONTENT)) as file:
                    data = file.readlines()
                for d in data:
                    content += str(d)
                    
            if entry.name.endswith(FILE_NAME):
                with open(os.path.join(name, FILE_NAME)) as file:
                    data = file.readlines()
                path = data[0].replace("\n","")

        con.execute("INSERT INTO data VALUES (?,?,?,?)", (path, document, pwd, content))
        con.commit()
        
con.close()