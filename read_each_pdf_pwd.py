import os
from configuration import current_directory, output_dir, TYPES, FILE_MAIL, FILE_INFO

## review each file inside Salida
names = [ os.path.join(output_dir,f"{x}") for x in os.listdir(output_dir)]
for name in names:
    with os.scandir(name) as it:
        for entry in it:
            read_data = None
            if entry.name.endswith(FILE_MAIL):
                with open(entry.path, "r") as file:
                    read_data = file.read()
                
                text = None
                pwd = None
                
                if read_data:
                    read_data.replace("\n", " ")
                    ## Type of document
                    type_document = read_data.split("<https://clientes.firmaautografa.com/img/concluido_papers.png>")[1]
                    text = type_document.split("\n\n\n")[0]
                    for t in TYPES:
                        if t in text:
                            text = t
                    pwd = type_document.split("Password: ")[1].split(" \n\n\n")[0]
                
                path_to_write = os.path.join(name,FILE_INFO)
                info_to_write = name + "\n" + text + "\n" + pwd + "\n"
                
                with open(path_to_write, "w") as f:
                    f.write(info_to_write)