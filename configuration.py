import os

## You can change this information
OUTPUT_DIRNAME = "retrieve-mails"
ANALYZE_DIRNAME = "mails.pst"
FILE_MAIL = "email_body.txt"
FILE_INFO = "info.txt"
FILE_CONTENT = "extract.txt"
FILE_NAME = "name_document.txt"
TYPES = ["Solicitud Grupal 140",
         "Solicitud Individual 138",
         "Cuestionario RA",
         "Contrato Multiple de Declaraciones"]
DATABASE_NAME = "result_emails.db"

## Please, don't change this information
current_directory = os.path.dirname(os.path.abspath(__file__))
output_dir = os.path.join(current_directory, OUTPUT_DIRNAME)
if not os.path.exists(output_dir):
    os.mkdir(output_dir)
read_dir = os.path.join(current_directory, ANALYZE_DIRNAME)